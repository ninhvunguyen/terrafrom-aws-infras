# The terraform-aws-infras hierachy
```
.
├── envs
│   ├── dev
│   ├── prod
│   └── stg
└── modules
    ├── eks-cluster
    ├── eks-node-group
    ├── rds
    ├── subnet
    └── vpc
        └── modules
            └── vpc-endpoints
```
# Description
- All environment variables will be put into corresponding folder [dev] [stg] [prod].
- The re-usable modules will be put into shared folder [modules], we can invoke them across multiple environment without duplicated
- The Terraform will provision 02 VPC for EKS cluster and RDS, then create VPC peering
- The EKS cluster will be created on EKS VPC including node-group with 02 worker nodes
- The RDS MySQL instance will be created on RDS VPC

# How to use:
Example: provisioning infra for DEV environment:
## Prequisite:
- Update your backend for Terraform state at file: ./envs/dev/version.tf
```
  backend "s3" {
    bucket = "demo12mar"
    key    = "demo/test-state"
    region = "eu-central-1"
  }
```
-Update your AWS credentials using commmand: `aws configuration`, so that Terraform can talk to AWS API.

## Excecution
```
cd ./envs/dev
terraform init
terraform plan -var-file=value-dev.tfvars
terraform apply -var-file=fvalue-dev.tfvars -auto-approve
```

# Nginx Ingress Operator
Following the guide here: https://github.com/nginxinc/nginx-ingress-helm-operator/blob/main/docs/manual-installation.md to deploy the Operator and associated resources:
- Clone the nginx-ingress-operator repo
```
git clone https://github.com/nginxinc/nginx-ingress-helm-operator/ --branch v2.1.2
cd nginx-ingress-helm-operator/
```
- To deploy the Operator and associated resources to all environments, run:
```
make deploy IMG=nginx/nginx-ingress-operator:2.1.2
```
- Check that the Operator is running:
```
kubectl get deployments -n nginx-ingress-operator-system
NAME                                        READY   UP-TO-DATE   AVAILABLE   AGE
nginx-ingress-operator-controller-manager   1/1     1            1           15s
```
- Install Nginx Ingress Controller
```
kubectl -n kube-system  apply -f https://raw.githubusercontent.com/nginxinc/nginx-ingress-helm-operator/main/config/samples/charts_v1alpha1_nginxingress.yaml
```
