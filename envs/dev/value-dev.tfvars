
##VPC
region = "eu-central-1"

availability_zones = ["eu-central-1a", "eu-central-1b"]

namespace = "demo"

stage = "test"

name = "lab"

default_security_group_deny_all       = true
default_route_table_no_routes         = true
default_network_acl_deny_all          = true
network_address_usage_metrics_enabled = true 



##RDS

deletion_protection = false

database_name = "test_db"

database_user = "admin"

database_password = "password"

database_port = 3306

multi_az = false

storage_type = "standard"

storage_encrypted = false

allocated_storage = 10

engine = "mysql"

engine_version = "5.7"

major_engine_version = "5"

instance_class = "db.t3.medium"

db_parameter_group = "mysql5.7"

publicly_accessible = false

apply_immediately = true


###EKS-NODE-GROUP

# oidc_provider_enabled is required to be true for VPC CNI addon
oidc_provider_enabled = true

enabled_cluster_log_types = ["audit"]

cluster_log_retention_period = 7

instance_types = ["t3.small"]

desired_size = 2

max_size = 3

min_size = 2

kubernetes_labels = {}

cluster_encryption_config_enabled = true

# When updating the Kubernetes version, also update the API and client-go version in test/src/go.mod
kubernetes_version = "1.29"

private_ipv6_enabled = false

addons = [
  # https://docs.aws.amazon.com/eks/latest/userguide/managing-kube-proxy.html
  {
    addon_name                  = "kube-proxy"
    addon_version               = null
    resolve_conflicts_on_create = "OVERWRITE"
    resolve_conflicts_on_update = "PRESERVE"
    service_account_role_arn    = null
  },
  # https://docs.aws.amazon.com/eks/latest/userguide/managing-coredns.html
  {
    addon_name               = "coredns"
    addon_version            = null
    resolve_conflicts        = "NONE"
    service_account_role_arn = null
  },
]
