###VPC
variable "region" {
  type = string
}

variable "availability_zones" {
  type = list(string)
}

variable "default_security_group_deny_all" {
  type = bool
}

variable "default_route_table_no_routes" {
  type = bool
}

variable "default_network_acl_deny_all" {
  type = bool
}

variable "network_address_usage_metrics_enabled" {
  type = bool
}


###RDS
variable "database_name" {
  type        = string
  description = "The name of the database to create when the DB instance is created"
}

variable "database_user" {
  type        = string
  description = "Username for the primary DB user"
}

variable "database_password" {
  type        = string
  description = "Password for the primary DB user"
}

variable "database_port" {
  type        = number
  description = "Database port (_e.g._ `3306` for `MySQL`). Used in the DB Security Group to allow access to the DB instance from the provided `security_group_ids`"
}

variable "deletion_protection" {
  type        = bool
  description = "Set to true to enable deletion protection on the RDS instance"
}

variable "multi_az" {
  type        = bool
  description = "Set to true if multi AZ deployment must be supported"
}

variable "availability_zone" {
  type        = string
  default     = null
  description = "The AZ for the RDS instance. Specify one of `subnet_ids`, `db_subnet_group_name` or `availability_zone`. If `availability_zone` is provided, the instance will be placed into the default VPC or EC2 Classic"
}

variable "db_subnet_group_name" {
  type        = string
  default     = null
  description = "Name of DB subnet group. DB instance will be created in the VPC associated with the DB subnet group. Specify one of `subnet_ids`, `db_subnet_group_name` or `availability_zone`"
}

variable "storage_type" {
  type        = string
  description = "One of 'standard' (magnetic), 'gp2' (general purpose SSD), 'gp3' (general purpose SSD), or 'io1' (provisioned IOPS SSD)"
}

variable "storage_encrypted" {
  type        = bool
  description = "(Optional) Specifies whether the DB instance is encrypted. The default is false if not specified"
}

variable "allocated_storage" {
  type        = number
  description = "The allocated storage in GBs"
}

variable "engine" {
  type        = string
  description = "Database engine type"
  # http://docs.aws.amazon.com/cli/latest/reference/rds/create-db-instance.html
  # - mysql
  # - postgres
  # - oracle-*
  # - sqlserver-*
}

variable "engine_version" {
  type        = string
  description = "Database engine version, depends on engine type"
  # http://docs.aws.amazon.com/cli/latest/reference/rds/create-db-instance.html
}

variable "major_engine_version" {
  type        = string
  description = "Database MAJOR engine version, depends on engine type"
  # https://docs.aws.amazon.com/cli/latest/reference/rds/create-option-group.html
}

variable "instance_class" {
  type        = string
  description = "Class of RDS instance"
  # https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Concepts.DBInstanceClass.html
}

variable "db_parameter_group" {
  type        = string
  description = "Parameter group, depends on DB engine used"
  # "mysql5.6"
  # "postgres9.5"
}

variable "publicly_accessible" {
  type        = bool
  description = "Determines if database can be publicly available (NOT recommended)"
}

variable "apply_immediately" {
  type        = bool
  description = "Specifies whether any database modifications are applied immediately, or during the next maintenance window"
}



###EKS-NODE-GROUP

variable "kubernetes_version" {
  type        = string
  default     = "1.29"
  description = "Desired Kubernetes master version. If you do not specify a value, the latest available version is used"
}

variable "enabled_cluster_log_types" {
  type        = list(string)
  default     = []
  description = "A list of the desired control plane logging to enable. For more information, see https://docs.aws.amazon.com/en_us/eks/latest/userguide/control-plane-logs.html. Possible values [`api`, `audit`, `authenticator`, `controllerManager`, `scheduler`]"
}

variable "cluster_log_retention_period" {
  type        = number
  default     = 0
  description = "Number of days to retain cluster logs. Requires `enabled_cluster_log_types` to be set. See https://docs.aws.amazon.com/en_us/eks/latest/userguide/control-plane-logs.html."
}

variable "oidc_provider_enabled" {
  type        = bool
  default     = true
  description = "Create an IAM OIDC identity provider for the cluster, then you can create IAM roles to associate with a service account in the cluster, instead of using `kiam` or `kube2iam`. For more information, see https://docs.aws.amazon.com/eks/latest/userguide/enable-iam-roles-for-service-accounts.html"
}

variable "instance_types" {
  type        = list(string)
  description = "Set of instance types associated with the EKS Node Group. Defaults to [\"t3.medium\"]. Terraform will only perform drift detection if a configuration value is provided"
}

variable "kubernetes_labels" {
  type        = map(string)
  description = "Key-value mapping of Kubernetes labels. Only labels that are applied with the EKS API are managed by this argument. Other Kubernetes labels applied to the EKS Node Group will not be managed"
  default     = {}
}

variable "desired_size" {
  type        = number
  description = "Desired number of worker nodes"
}

variable "max_size" {
  type        = number
  description = "The maximum size of the AutoScaling Group"
}

variable "min_size" {
  type        = number
  description = "The minimum size of the AutoScaling Group"
}

variable "cluster_encryption_config_enabled" {
  type        = bool
  default     = true
  description = "Set to `true` to enable Cluster Encryption Configuration"
}

variable "cluster_encryption_config_kms_key_id" {
  type        = string
  default     = ""
  description = "KMS Key ID to use for cluster encryption config"
}

variable "cluster_encryption_config_kms_key_enable_key_rotation" {
  type        = bool
  default     = true
  description = "Cluster Encryption Config KMS Key Resource argument - enable kms key rotation"
}

variable "cluster_encryption_config_kms_key_deletion_window_in_days" {
  type        = number
  default     = 10
  description = "Cluster Encryption Config KMS Key Resource argument - key deletion windows in days post destruction"
}

variable "cluster_encryption_config_kms_key_policy" {
  type        = string
  default     = null
  description = "Cluster Encryption Config KMS Key Resource argument - key policy"
}

variable "cluster_encryption_config_resources" {
  type        = list(any)
  default     = ["secrets"]
  description = "Cluster Encryption Config Resources to encrypt, e.g. ['secrets']"
}

variable "addons" {
  type = list(object({
    addon_name    = string
    addon_version = string
    # resolve_conflicts is deprecated, but we keep it for backwards compatibility
    # and because if not declared, Terraform will silently ignore it.
    resolve_conflicts           = optional(string, null)
    resolve_conflicts_on_create = optional(string, null)
    resolve_conflicts_on_update = optional(string, null)
    service_account_role_arn    = string
  }))
  default     = []
  description = "Manages [`aws_eks_addon`](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_addon) resources."
}

variable "private_ipv6_enabled" {
  type        = bool
  default     = false
  description = "Whether to use IPv6 addresses for the pods in the node group"
}


####EKS-CLUSTER
#variable "kubernetes_version" {
#  type        = string
#  default     = "1.29"
#  description = "Desired Kubernetes master version. If you do not specify a value, the latest available version is used"
#}
#
#
#
####EKS-WOKERS
#
#variable "cluster_name" {
#  type        = string
#  description = "The name of the EKS cluster"
#  default     ="demo-test-lab-cluster"
#}
#
#variable "cluster_endpoint" {
#  type        = string
#  description = "EKS cluster endpoint"
#}
#
#variable "cluster_certificate_authority_data" {
#  type        = string
#  description = "The base64 encoded certificate data required to communicate with the cluster"
#}
#
#variable "cluster_security_group_ingress_enabled" {
#  type        = bool
#  description = "Whether to enable the EKS cluster Security Group as ingress to workers Security Group"
#}
#
#variable "cluster_security_group_id" {
#  type        = string
#  description = "Security Group ID of the EKS cluster"
#  default     = ""
#}
#
#variable "instance_type" {
#  type        = string
#  description = "Instance type to launch"
#}
#
#variable "max_size" {
#  type        = number
#  description = "The maximum size of the autoscale group"
#}
#
#variable "min_size" {
#  type        = number
#  description = "The minimum size of the autoscale group"
#}
#
#variable "health_check_type" {
#  type        = string
#  description = "Controls how health checking is done. Valid values are `EC2` or `ELB`"
#}
#
#variable "wait_for_capacity_timeout" {
#  type        = string
#  description = "A maximum duration that Terraform should wait for ASG instances to be healthy before timing out. Setting this to #'0' causes Terraform to skip all Capacity Waiting behavior"
#}
#
#variable "autoscaling_policies_enabled" {
#  type        = bool
#  description = "Whether to create `aws_autoscaling_policy` and `aws_cloudwatch_metric_alarm` resources to control Auto Scaling"
#}
#
#variable "cpu_utilization_high_threshold_percent" {
#  type        = number
#  description = "The value against which the specified statistic is compared"
#}
#
#variable "cpu_utilization_low_threshold_percent" {
#  type        = number
#  description = "The value against which the specified statistic is compared"
#}#