##VPC_EKS

output "public_subnet_eks_cidrs" {
  value = module.subnets.public_subnet_cidrs
}

output "private_subnet_eks_cidrs" {
  value = module.subnets.private_subnet_cidrs
}

output "vpc_eks_cidr" {
  value = module.vpc.vpc_cidr_block
}

output "additional_eks_cidr_blocks" {
  description = "A list of the additional IPv4 CIDR blocks associated with the VPC"
  value       = module.vpc.additional_cidr_blocks
}

output "additional_eks_cidr_blocks_to_association_ids" {
  description = "A map of the additional IPv4 CIDR blocks to VPC CIDR association IDs"
  value       = module.vpc.additional_cidr_blocks_to_association_ids
}

output "vpc_eks_ipv6_association_id" {
  value       = module.vpc.vpc_ipv6_association_id
  description = "The association ID for the primary IPv6 CIDR block"
}

output "vpc_eks_ipv6_cidr_block" {
  value       = module.vpc.vpc_ipv6_cidr_block
  description = "The primary IPv6 CIDR block"
}

output "ipv6_eks_cidr_block_network_border_group" {
  value       = module.vpc.ipv6_cidr_block_network_border_group
  description = "The Network Border Group Zone name"
}

##VPC_RDS

output "public_subnet_rds_cidrs" {
  value = module.subnets-rds.public_subnet_cidrs
}

output "private_subnet_rds_cidrs" {
  value = module.subnets-rds.private_subnet_cidrs
}

output "vpc_rds_cidr" {
  value = module.vpc-rds.vpc_cidr_block
}

output "additional_rds_cidr_blocks" {
  description = "A list of the additional IPv4 CIDR blocks associated with the VPC"
  value       = module.vpc-rds.additional_cidr_blocks
}

output "additional_rds_cidr_blocks_to_association_ids" {
  description = "A map of the additional IPv4 CIDR blocks to VPC CIDR association IDs"
  value       = module.vpc-rds.additional_cidr_blocks_to_association_ids
}

output "vpc_rds_ipv6_association_id" {
  value       = module.vpc-rds.vpc_ipv6_association_id
  description = "The association ID for the primary IPv6 CIDR block"
}

output "vpc_rds_ipv6_cidr_block" {
  value       = module.vpc-rds.vpc_ipv6_cidr_block
  description = "The primary IPv6 CIDR block"
}

output "ipv6_rds_cidr_block_network_border_group" {
  value       = module.vpc.ipv6_cidr_block_network_border_group
  description = "The Network Border Group Zone name"
}

##VPC_RDS SUBNETs

output "public_subnet_rds_ipv6_cidrs" {
  description = "IPv6 CIDRs assigned to the created public subnets"
  value       = module.subnets-rds.public_subnet_ipv6_cidrs
}
output "private_subnet_rds_ipv6_cidrs" {
  description = "IPv6 CIDRs assigned to the created private subnets"
  value       = module.subnets-rds.private_subnet_ipv6_cidrs
}
output "public_rds_route_table_ids" {
  description = "IDs of the created public route tables"
  value       = module.subnets-rds.public_route_table_ids
}

output "private_rds_route_table_ids" {
  description = "IDs of the created private route tables"
  value       = module.subnets-rds.private_route_table_ids
}

##VPC_EKS SUBNETs

output "public_subnet_eks_ipv6_cidrs" {
  description = "IPv6 CIDRs assigned to the created public subnets"
  value       = module.subnets.public_subnet_ipv6_cidrs
}

output "private_subnet_eks_ipv6_cidrs" {
  description = "IPv6 CIDRs assigned to the created private subnets"
  value       = module.subnets.private_subnet_ipv6_cidrs
}
output "public_eks_route_table_ids" {
  description = "IDs of the created public route tables"
  value       = module.subnets.public_route_table_ids
}

output "private_eks_route_table_ids" {
  description = "IDs of the created private route tables"
  value       = module.subnets.private_route_table_ids
}


###RDS

output "instance_id" {
  value       = module.rds_instance.instance_id
  description = "ID of the resource"
}

output "instance_address" {
  value       = module.rds_instance.instance_address
  description = "Address of the instance"
}

output "instance_endpoint" {
  value       = module.rds_instance.instance_endpoint
  description = "DNS Endpoint of the instance"
}

output "subnet_group_id" {
  value       = module.rds_instance.subnet_group_id
  description = "ID of the created Subnet Group"
}

output "security_group_id" {
  value       = module.rds_instance.security_group_id
  description = "ID of the Security Group"
}

output "parameter_group_id" {
  value       = module.rds_instance.parameter_group_id
  description = "ID of the Parameter Group"
}

output "option_group_id" {
  value       = module.rds_instance.option_group_id
  description = "ID of the Option Group"
}

output "hostname" {
  value       = module.rds_instance.hostname
  description = "DNS host name of the instance"
}

###EKS

output "public_subnet_cidrs" {
  value       = module.subnets.public_subnet_cidrs
  description = "Public subnet CIDRs"
}

output "private_subnet_cidrs" {
  value       = module.subnets.private_subnet_cidrs
  description = "Private subnet CIDRs"
}

output "vpc_cidr" {
  value       = module.vpc.vpc_cidr_block
  description = "VPC ID"
}

output "eks_cluster_id" {
  description = "The name of the cluster"
  value       = module.eks_cluster.eks_cluster_id
}

output "eks_cluster_arn" {
  description = "The Amazon Resource Name (ARN) of the cluster"
  value       = module.eks_cluster.eks_cluster_arn
}

output "eks_cluster_endpoint" {
  description = "The endpoint for the Kubernetes API server"
  value       = module.eks_cluster.eks_cluster_endpoint
}

output "eks_cluster_version" {
  description = "The Kubernetes server version of the cluster"
  value       = module.eks_cluster.eks_cluster_version
}

output "eks_cluster_identity_oidc_issuer" {
  description = "The OIDC Identity issuer for the cluster"
  value       = module.eks_cluster.eks_cluster_identity_oidc_issuer
}

output "eks_cluster_managed_security_group_id" {
  description = "Security Group ID that was created by EKS for the cluster. EKS creates a Security Group and applies it to ENI that is attached to EKS Control Plane master nodes and to any managed workloads"
  value       = module.eks_cluster.eks_cluster_managed_security_group_id
}

output "eks_cluster_ipv6_service_cidr" {
  description = <<-EOT
    The IPv6 CIDR block that Kubernetes pod and service IP addresses are assigned from
    if `kubernetes_network_ipv6_enabled` is set to true. If set to false this output will be null.
    EOT
  value       = module.eks_cluster.eks_cluster_ipv6_service_cidr
}

output "eks_node_group_role_arn" {
  description = "ARN of the worker nodes IAM role"
  value       = module.eks_node_group.eks_node_group_role_arn
}

output "eks_node_group_role_name" {
  description = "Name of the worker nodes IAM role"
  value       = module.eks_node_group.eks_node_group_role_name
}

output "eks_node_group_id" {
  description = "EKS Cluster name and EKS Node Group name separated by a colon"
  value       = module.eks_node_group.eks_node_group_id
}

output "eks_node_group_arn" {
  description = "Amazon Resource Name (ARN) of the EKS Node Group"
  value       = module.eks_node_group.eks_node_group_arn
}

output "eks_node_group_resources" {
  description = "List of objects containing information about underlying resources of the EKS Node Group"
  value       = module.eks_node_group.eks_node_group_resources
}

output "eks_node_group_status" {
  description = "Status of the EKS Node Group"
  value       = module.eks_node_group.eks_node_group_status
}
